const express = require("express");
const kana = require("./data/kana.json");

let app = express();

app.set("views", "./views");
app.set("view engine", "pug");

app.use(express.static("public"));

app.get("/", (req, res) => {
	res.render("index", { title: "Kana API", description: "Kana (Japanese syllabaries writing system) API" });
});
app.get("/api", (req, res) => {
	let dataKana = JSON.stringify(kana);
	res.header("Content-Type", "application/json");
	res.send(dataKana);
});

const listener = app.listen(3000, () => {
	console.log(`App is listening on port ${listener.address().port}`);
});
